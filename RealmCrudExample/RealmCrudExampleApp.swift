//
//  RealmCrudExampleApp.swift
//  RealmCrudExample
//
//  Created by Manon Salsou and cano
//

import SwiftUI

@main
struct RealmCrudExampleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
